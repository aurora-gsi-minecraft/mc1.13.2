package wibble.mods.auroraGsi;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.GuiControls;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.Potion;
import net.minecraftforge.client.event.RenderGameOverlayEvent;

/** Container for the Provider data and Player data. */
public class GSINode {
    private ProviderNode provider = new ProviderNode();
    private GameNode game = new GameNode();
    private WorldNode world = new WorldNode();
    private PlayerNode player = new PlayerNode();

    public GSINode update() {
        game.update();
        world.update();
        player.update();
        return this;
    }

    /**
     * Contains data required for Aurora to be able to parse the JSON data.
     */
    private static class ProviderNode {
        private String name = "minecraft";
        private int appid = -1;
    }

    /**
     * Contains the data extracted from the game about the player.
     */
    private static class PlayerNode {
        private boolean inGame;
        private float health;
        private float maxHealth;
        private float absorption;
        private boolean isDead;
        private int armor;
        private int experienceLevel;
        private float experience;
        private int foodLevel;
        private float saturationLevel;
        private boolean isSneaking;
        private boolean isRidingHorse;
        private boolean isBurning;
        private boolean isInWater;
        private HashMap<String, Boolean> playerEffects = new HashMap<>();

        // Potion effects that will be added to the playerEffects map.
        private static final HashMap<String, Potion> TARGET_POTIONS;
        static {
            TARGET_POTIONS = new HashMap<>();
            TARGET_POTIONS.put("moveSpeed", MobEffects.SPEED);
            TARGET_POTIONS.put("moveSlowdown", MobEffects.SLOWNESS);
            TARGET_POTIONS.put("haste", MobEffects.HASTE);
            TARGET_POTIONS.put("miningFatigue", MobEffects.MINING_FATIGUE);
            TARGET_POTIONS.put("strength", MobEffects.STRENGTH);
            //TARGET_POTIONS.put("instantHealth", INSTANT_HEALTH);
            //TARGET_POTIONS.put("instantDamage", INSTANT_DAMAGE);
            TARGET_POTIONS.put("jumpBoost", MobEffects.JUMP_BOOST);
            TARGET_POTIONS.put("confusion", MobEffects.NAUSEA);
            TARGET_POTIONS.put("regeneration", MobEffects.REGENERATION);
            TARGET_POTIONS.put("resistance", MobEffects.RESISTANCE);
            TARGET_POTIONS.put("fireResistance", MobEffects.FIRE_RESISTANCE);
            TARGET_POTIONS.put("waterBreathing", MobEffects.WATER_BREATHING);
            TARGET_POTIONS.put("invisibility", MobEffects.INVISIBILITY);
            TARGET_POTIONS.put("blindness", MobEffects.BLINDNESS);
            TARGET_POTIONS.put("nightVision", MobEffects.NIGHT_VISION);
            TARGET_POTIONS.put("hunger", MobEffects.HUNGER);
            TARGET_POTIONS.put("weakness", MobEffects.WEAKNESS);
            TARGET_POTIONS.put("poison", MobEffects.POISON);
            TARGET_POTIONS.put("wither", MobEffects.WITHER);
            //TARGET_POTIONS.put("healthBoost", MobEffects.HEALTH_BOOST);
            TARGET_POTIONS.put("absorption", MobEffects.ABSORPTION);
            //TARGET_POTIONS.put("saturation", MobEffects.SATURATION);
            TARGET_POTIONS.put("glowing", MobEffects.GLOWING);
            TARGET_POTIONS.put("levitation", MobEffects.LEVITATION);
            TARGET_POTIONS.put("luck", MobEffects.LUCK);
            TARGET_POTIONS.put("badLuck", MobEffects.UNLUCK);
            TARGET_POTIONS.put("slowFalling", MobEffects.SLOW_FALLING);
            TARGET_POTIONS.put("conduitPower", MobEffects.CONDUIT_POWER);
            TARGET_POTIONS.put("dolphinsGrace", MobEffects.DOLPHINS_GRACE);
        }

        private void update() {
            try {
                playerEffects.clear(); // clear before attempting to get the player else there may be values on the mainmenu

                // Attempt to get a player, and store their health and stuff
                EntityPlayerSP player = Minecraft.getInstance().player;
                health = player.getHealth();
                maxHealth = player.getMaxHealth();
                absorption = player.getAbsorptionAmount();
                isDead = !player.isAlive();
                armor = player.getTotalArmorValue();
                experienceLevel = player.experienceLevel;
                experience = player.experience;
                foodLevel = player.getFoodStats().getFoodLevel();
                saturationLevel = player.getFoodStats().getSaturationLevel();
                isSneaking = player.isSneaking();
                isRidingHorse = player.isRidingHorse();
                isBurning = player.isBurning();
                isInWater = player.isInWater();

                // TODO: add boolean for chat open `Minecraft.getMinecraft().ingameGUI.getChatGUI().getChatOpen();`

                // Populate the player's effect map
                for (Map.Entry<String, Potion> potion : TARGET_POTIONS.entrySet())
                    playerEffects.put(potion.getKey(), player.getActivePotionEffect(potion.getValue()) != null);

                inGame = true;

            } catch (Exception ex) {
                // If this failed (I.E. could not get a player, the user is probably not in a game)
                inGame = false;
            }
        }
    }

    /**
     * Contains the data extracted from the game about the current world.
     */
    private static class WorldNode {
        private long worldTime;
        private boolean isDayTime;
        private boolean isRaining;
        private float rainStrength;
        private int dimensionID;

        private void update() {
            try {
                WorldClient world = Minecraft.getInstance().world;
                worldTime = world.getDayTime();
                isDayTime = world.isDaytime();
                rainStrength = world.rainingStrength;
                isRaining = world.isRaining();
                dimensionID = world.dimension.getType().getId();
            } catch (Exception ignore) { }
        }
    }

    /**
     * Contains lists of any used keys and any that are conflicting.
     */
    private static class GameNode {
        private KeyBinding[] keys;
        private boolean controlsGuiOpen;
        private boolean chatGuiOpen;

        private void update() {
            Minecraft mc = Minecraft.getInstance();
            controlsGuiOpen = mc.currentScreen instanceof GuiControls;
            chatGuiOpen = mc.currentScreen instanceof GuiChat;
            keys = null;
            if(controlsGuiOpen)
                keys = mc.gameSettings.keyBindings;
        }
    }
}